# frozen_string_literal: true

require 'sinatra'
require 'sinatra/config_file'
require 'optparse'
require 'redcarpet'
require 'rouge'
require 'erb'
require 'open3'
require 'rouge/plugins/redcarpet'
require 'webrick/https'
require 'openssl'
require 'securerandom'
require 'cgi'

class HtmlWithRouge < Redcarpet::Render::HTML
  include Rouge::Plugins::Redcarpet
end

GREP_COMMAND = system('which', 'rg') ? ['rg', '--no-heading', '-nP'] : ['grep', '-rnP']

class PrettyDirectoryServer < Sinatra::Base
  register Sinatra::ConfigFile
  config_file 'config.yml'

  server_username = ENV['PDS_BASIC_AUTH_USERNAME']
  server_password = ENV['PDS_BASIC_AUTH_PASSWORD']
  unless server_username.nil? || server_password.nil?
    use Rack::Auth::Basic, 'Restricted Area' do |username, password|
      username == server_username && password == server_password
    end
  end

  def split_entries(path)
    files = []
    dirs = []
    Dir.entries(path).sort.each do |entry|
      if File.directory?(File.join(path, entry))
        dirs.push(entry)
      else
        files.push(entry)
      end
    end

    [files, dirs]
  end

  def render_code(code, language, menu_data)
    renderer = Redcarpet::Markdown.new(HtmlWithRouge, no_intra_emphasis: true, tables: true, fenced_code_blocks: true)
    md = renderer.render("```#{language}
  #{code}
  ```")

    erb :code, locals: { code: md, menu_data: menu_data }
  end

  def render_code_file(path, language, menu_data)
    if params['prettify'] == 'true'
      out, err = Open3.capture3('prettier', path)
      code = err.empty? ? out : err
    else
      code = File.read(path)
    end
    render_code(code, language, menu_data)
  end

  helpers do
    def url_encode(str)
      ERB::Util.url_encode(str)
    end
  end

  get '/find_file' do
    dir = params['parent_dir']
    filename = params['filename']

    if dir.nil? || filename.nil?
      halt 400
    else
      root = File.expand_path(settings.directory)
      path = Dir.glob("#{root}/**/#{dir}/**/#{filename}").first
      if path.nil?
        halt 404, 'File does not exist'
      else
        redirect path.gsub(root, '').to_s
      end
    end
  end

  get '/highlight.css' do
    headers 'Content-Type' => 'text/css'
    Rouge::Themes::Base16.mode(:dark).render(scope: '.highlight')
  end

  get '/search' do
    query = params['query']
    halt 400, 'Missing query' if query.nil? || query.empty?

    base = params['base']
    halt 400, 'Missing base directory' if base.nil? || base.empty?

    path = File.join(File.expand_path(settings.directory), base)
    halt 400, 'Invalid base directory' unless Dir.exist?(path)

    cmd = [*GREP_COMMAND]
    cmd.push('-o') if params['show_only_match']
    cmd.push('--', query, path)
    begin
      out, err = Open3.capture3(*cmd)
    rescue StandardError => e
      halt 500, e
    end

    halt 500, "search error: #{err}" unless err.empty?

    render_code("# Ruby command that ran: Open3.capture3(*#{cmd})\n#{out}", 'plaintext', nil)
  end

  get '/*' do |sub_path|
    root = File.expand_path(settings.directory)
    path = File.join(root, sub_path)

    halt 400, 'Invalid path' unless path.start_with?(root)

    if File.directory?(path)
      files, dirs = split_entries(path)
      erb :directory_listing, locals: { sub_path: request.path_info, base_dir: root, files: files, dirs: dirs }
    elsif File.exist?(path)
      raw = params['raw'] == 'true'
      diff = params['show_diff'] == 'true'
      serve = params['serve'] == 'true'
      history = (params['history'] || '0').to_i

      parent = File.dirname(path)
      relative_parent = parent.gsub(root, '')
      files, = split_entries(parent)
      current_file_position = files.index(File.basename(path))
      menu_data = {
        previous: File.join(relative_parent, files[current_file_position - 1]),
        parent: relative_parent.empty? ? '/' : relative_parent,
        next: File.join(relative_parent, files[(current_file_position + 1) % files.length]),
        current_path: path,
        in_diff_mode: diff,
        history: history
      }

      if raw || serve
        content_type 'text/plain' if raw
        headers 'Referrer-Policy' => 'no-referrer' if serve
        send_file path
      elsif diff
        _, o, e, t = Open3.popen3('git', 'log', '-1', '-p', "--skip=#{history}", '--', File.basename(path), chdir: File.dirname(path))
        if t.value.exitstatus.nil? || t.value.exitstatus.zero?
          render_code(o.read, 'diff', menu_data)
        else
          halt 500, "`git diff` error: #{e.read}"
        end
      elsif path.end_with?('.md.template')
        template_content = File.read(path)
        erb :template, locals: { markdown: template_content, placeholders: template_content.scan(/\{\{([^}]+)\}\}/).flatten.uniq }
      else
        ext = File.extname(path).downcase
        case ext
        when '.md'
          erb :code, locals: { code: markdown(File.read(path)), menu_data: menu_data }
        when '.rb'
          render_code_file(path, 'ruby', menu_data)
        when '.yml'
          render_code_file(path, 'yaml', menu_data)
        when '.js'
          render_code_file(path, 'javascript', menu_data)
        when '.png', 'jpg'
          send_file path
        else
          begin
            render_code_file(path, ext[1..] || 'plaintext', menu_data)
          rescue StandardError
            content_type 'text/plain'
            send_file path
          end
        end
      end
    else
      halt 404, 'File does not exist'
    end
  end
end

options = {
  Port: 1234,
  Logger: WEBrick::Log.new($stderr, WEBrick::Log::DEBUG),
  DocumentRoot: '/ruby/htdocs',
  SSLEnable: true,
  SSLVerifyClient: OpenSSL::SSL::VERIFY_NONE,
  Host: '0.0.0.0'
}

if ENV['SSL_CERT_PATH'] && ENV['SSL_KEY_PATH']
  options[:SSLCertificate] = OpenSSL::X509::Certificate.new(File.read(ENV['SSL_CERT_PATH']))
  options[:SSLPrivateKey] = OpenSSL::PKey::RSA.new(File.read(ENV['SSL_KEY_PATH']))
else
  options[:SSLCertName] = [['CN', SecureRandom.hex(10)]]
end

Rack::Handler::WEBrick.run(PrettyDirectoryServer, options)
