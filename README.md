# Pretty directory server

This little project serves as a slightly better than default directory listing with some utils. I use this to view my recon data hosted on my VPS but you can do whatever you want with it.

Data is served over HTTPS (self-signed invalid cert, the idea is just to encryp the traffic) on port 1234.

## Instructions

- Clone the repo
- Run `bundle install` in the repository's directory
- Modify the `config.yml` file to set the directory you want to serve
- Optionally set `PDS_BASIC_AUTH_USERNAME` and `PDS_BASIC_AUTH_PASSWORD` environment variables to protect the sever with basic authentication
- Optionally install `prettier` (`npm install -g prettier`) to support the prettifying option.
- Run `ruby server.rb`
